package br.com.api.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		System.out.println(System.getenv("CORS_URL"));
		SpringApplication.run(Application.class, args);
	}

}
